const express = require("express");

var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/Lucky_Dice_Casino_User_DiceHistory", function (error) {
    if (error) throw error;
    console.log('Successfully connected');
})

const app = new express();

app.use(express.json());

app.use(express.urlencoded({
    urlencoded: true,
}));
// Khai báo thư viện path
const path = require("path");

// Khai báo sử dụng tài nguyên
app.use(express.static("views"));

// Import file model
const userModel = require("./app/models/userModel");
const diceHistoryModel = require("./app/models/diceHistoryModel");
const prizeModel = require("./app/models/prizeModel");
const voucherModel = require("./app/models/voucherModel");
const prizeHistoryModel = require("./app/models/prizeHistoryModel");
const voucherHistoryModel = require("./app/models/voucherHistoryModel");
const userRouter = require("./app/routers/userRouter");
const diceHistoryRouter = require("./app/routers/diceHistoryRouter");
const prizeRouter = require("./app/routers/prizeRouter");
const voucherRouter = require("./app/routers/voucherRouter");
const prizeHistoryRouter = require("./app/routers/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routers/voucherHistoryRouter");
const diceRouter = require("./app/routers/diceRouter");

const port = 3000;

app.use((req, res, next) => {
    console.log(new Date());
    next();
})

app.use((req, res, next) => {
    console.log(req.method);
    next();
})

app.get("/random-number", (req, res) => {
    var ranNum = Math.floor(Math.random() * 6) + 1;
    res.status(200).json({
        numberRandom: ranNum,
    })
})

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/pizza365index.html"));
})

app.get("/viewOrder.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/viewOrder.html"));
})

app.get("/pizza365_nhanvien.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/pizza365_nhanvien.html.html"));
})

app.get("/orderdetail.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/orderdetail.html.html"));
})

// import userRouter
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);
app.use("/", diceRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})