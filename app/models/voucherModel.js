// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const voucherSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        // unique: true
    },
    code: {
        type: String,
        unique: true,
        require: true,
    },
    discount: {
        type: Number,
        require: true,
    },
    note: {
        type: String,
        require: false,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Voucher", voucherSchema);