// import voucher model
const { mongoose, Schema } = require("mongoose");
const voucherModel = require("../models/voucherModel");

// get all vouchers
const getAllVoucher = (req, res) => {
    voucherModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getVoucherById = (req, res) => {
    let id = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới voucher
const createVoucher = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.code) {
        res.status(400).json({
            message: "code is require!",
        })
    }
    else if (isNaN(body.discount) && body.discount <= 0) {
        res.status(400).json({
            message: "discount is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let voucher = {
            _id: Schema.Types.ObjectId(),
            code: body.code,
            discount: body.discount,
        };
        voucherModel.create(voucher, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateVoucherById = (req, res) => {
    let id = req.params.voucherId;
    let body = req.body;

    if (!body.code) {
        res.status(400).json({
            message: "code is require!",
        })
    }
    else if (isNaN(body.discount) && body.discount <= 0) {
        res.status(400).json({
            message: "discount is invalid!",
        })
    }
    else {
        let voucher = {
            code: body.code,
            discount: body.discount,
        };
        voucherModel.findByIdAndUpdate(id, voucher, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteVoucherById = (req, res) => {
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        voucherModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllVoucher, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById };