// import user model
const { mongoose } = require("mongoose");
const userModel = require("../models/userModel");

// get all users
const getAllUser = (req, res) => {
    userModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getUserById = (req, res) => {
    let id = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        userModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới User
const createUser = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.username) {
        res.status(400).json({
            message: "fullname is require!",
        })
    }
    else if (!body.firstname) {
        res.status(400).json({
            message: "email is require!",
        })
    }
    else if (!body.lastname) {
        res.status(400).json({
            message: "address is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let user = {
            _id: mongoose.Types.ObjectId(),
            username: body.username,
            firstname: body.firstname,
            lastname: body.lastname,
        };
        userModel.create(user, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateUserById = (req, res) => {
    let id = req.params.userId;
    let body = req.body;

    if (!body.username) {
        res.status(400).json({
            message: "fullname is require!",
        })
    }
    else if (!body.firstname) {
        res.status(400).json({
            message: "email is require!",
        })
    }
    else if (!body.lastname) {
        res.status(400).json({
            message: "address is require!",
        })
    }
    else {
        let user = {
            username: body.username,
            firstname: body.firstname,
            lastname: body.lastname,
        };
        userModel.findByIdAndUpdate(id, user, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteUserById = (req, res) => {
    let id = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        userModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllUser, getUserById, createUser, updateUserById, deleteUserById };