const express = require("express");
const prizeHistoryMiddleware = require("../middlewares/prizeHistoryMiddleware");
const prizeHistoryRouter = express.Router();

prizeHistoryRouter.use(prizeHistoryMiddleware);

// import các hàm module controller
const { getAllPrizeHistory, getPrizeHistoryById, createPrizeHistory, updatePrizeHistoryById, deletePrizeHistoryById } = require("../controllers/prizeHistoryController");

prizeHistoryRouter.get("/prize-histories", getAllPrizeHistory);

prizeHistoryRouter.get("/prize-histories/:historyId", getPrizeHistoryById);

prizeHistoryRouter.put("/prize-histories/:historyId", updatePrizeHistoryById);

prizeHistoryRouter.post("/prize-histories", createPrizeHistory);

prizeHistoryRouter.delete("/prize-histories/:historyId", deletePrizeHistoryById);

module.exports = prizeHistoryRouter;