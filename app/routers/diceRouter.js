const express = require("express");
const diceMiddleware = require("../middlewares/diceMiddleware");
const diceRouter = express.Router();

diceRouter.use(diceMiddleware);

const { diceHandler, getDiceHistory, getPrizeHistory, getVoucherHistory } = require("../controllers/diceController");

diceRouter.post("/devcamp-lucky-dice/dice", diceHandler);

diceRouter.get("/devcamp-lucky-dice/dice-history", getDiceHistory);

diceRouter.get("/devcamp-lucky-dice/prize-history", getPrizeHistory);

diceRouter.get("/devcamp-lucky-dice/voucher-history", getVoucherHistory);

module.exports = diceRouter;