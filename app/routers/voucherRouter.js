const express = require("express");
const voucherMiddleware = require("../middlewares/voucherMiddleware");
const voucherRouter = express.Router();

voucherRouter.use(voucherMiddleware);

// import các hàm module controller
const { getAllVoucher, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById } = require("../controllers/voucherController");

voucherRouter.get("/vouchers", getAllVoucher);

voucherRouter.get("/vouchers/:voucherId", getVoucherById);

voucherRouter.put("/vouchers/:voucherId", updateVoucherById);

voucherRouter.post("/vouchers", createVoucher);

voucherRouter.delete("/vouchers/:voucherId", deleteVoucherById);

module.exports = voucherRouter;